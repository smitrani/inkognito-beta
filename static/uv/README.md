# ultraviolet-scripts
Core Ultraviolet scripts

# Configuration
Configure Ultraviolet for both client-hooking & service worker in `uv.config.js`
```javascript
self.__uv$config = {
    bare: '/bare/',
    prefix: '/service/',
    encodeUrl: Ultraviolet.codec.xor.encode,
    decodeUrl: Ultraviolet.codec.xor.decode,
    handler: 'static/uv.handler.js',
    bundle: 'static/uv.bundle.js',
    config: 'static/uv.config.js',
};
```


# Example Usage
```javascript
importScripts('/PATHTOSCRIPTS/uv.sw.js');

const sw = new UVServiceWorker();

self.addEventListener('fetch', event =>
    event.respondWith(
        sw.fetch(event)
    )
);
```
